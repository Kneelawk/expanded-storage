package ellemes.expandedstorage.common.block.strategies;

public interface ItemAccess {
    Object get();
}
